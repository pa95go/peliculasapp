
# Documentación Películas

### Instalación del proyecto:

##### 1. Instalamos el back-end:
- 1.1: Abrimos la terminal y accedemos a la direccion donde esta nuetro repositorio
- 1.2: $ cd PeliculasApp/peliculas-back
- 1.3: $ docker build -t peliculas-back .
- 1.4: $ docker run -it -p 3000:3000 peliculas-back


##### 2. Instalamos el front-end
- 2.1: Abrimos otra terminal  y accedemos a la direccion donde esta nuetro repositorio
- 2.2: $ cd cd PeliculasApp/peliculas
- 2.3: $ docker build -t peliculas .
- 2.4: $ docker run -it -p 4200:4200 peliculas
	
##### 3. Abrir el navegador con el enlace: http://localhost:4200

-----

Para detener los servidores locales del front-end y del back-end usamos el comando:
- $ docker stop $(docker ps -a -q)
	
-----

Nota: Si instalamos el proyecto en sistema operativo de linux, nos pedirá permisos, por eso necesitamos poner delante de docker ‘sudo’.

