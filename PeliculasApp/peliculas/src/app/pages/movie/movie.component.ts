import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent implements OnInit {

  id: string = "0";
  nombre: string = "d";
  datos = JSON.parse(String(localStorage.getItem("pelicula")));
  botonFav: string = "Añadir a favoritos";
  botonFavclass: string = "btn-outline-danger";
  listaFavoritas :string= ""
  constructor(public dataService: DataService) { 
    
  }
  
  ngOnInit(): void {
    if(localStorage.getItem("favoritas")!== null){

      this.listaFavoritas=String(localStorage.getItem("favoritas"));
    
      if((String(localStorage.getItem("favoritas")).split('-')).includes(String(this.datos['id']))){
        this.botonFav="FAVORITO";
        this.botonFavclass = "btn-danger";
      }
  }
  
    
  }

   addFavorito() {

       if((localStorage.getItem("favoritas")!== null)&&(!(String(localStorage.getItem("favoritas")).split('-')).includes(String(this.datos['id'])))){
         localStorage.setItem("favoritas", `${this.listaFavoritas}-${String(this.datos['id'])}`);
         this.botonFav="FAVORITO";
         this.botonFavclass = "btn-danger";
        }else {
          localStorage.setItem("favoritas", String(this.datos['id']));
          this.botonFav="FAVORITO";
          this.botonFavclass = "btn-danger";
        }
     
    
   
  }

}
