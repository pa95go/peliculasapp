import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponenstsModule } from '../components/componensts.module';

import { HomeComponent } from './home/home.component';
import { MovieComponent } from './movie/movie.component';



@NgModule({
  declarations: [
    HomeComponent,
    MovieComponent
  ],
  imports: [
    CommonModule,
    ComponenstsModule
  ]
})
export class PagesModule { }
