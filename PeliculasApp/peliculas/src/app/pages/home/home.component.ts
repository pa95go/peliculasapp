import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  nombre: string ='Nombre Pelicula';
  imagen: string ='Imagen Pelicula';
  id: string ='0';
  peliculasPagina: number = 1;
  numPaginas: number =1;
  texto:string = '(orden: mayor a menor)';
  peliculas = []

  constructor(public dataService: DataService) { }

  ngOnInit(): void {
    const url = 'http://localhost:3000/peliculas';
    // obtengo datos utilizando fetch
    fetch(url).then(response => response.json()).then(data => {
      this.peliculas = data.results; // <-- asigno los valores a la propiedad del componente
      this.numPaginas = (this.peliculas.length/20);
   
      this.peliculas.sort((a, b) => (a['vote_count'] > b['vote_count'] ? -1 : 1));
   
      
    });


  }
  
 
  filtrosTopPeliculas(){

    this.texto = '(filtros: +4 estrellas y no aparece Keanu Reeves) (orden: mayor a menor)'
    this.peliculasPagina= 1;
    //Filtro 1: Peliculas que tengan más de 4 estrellas. (Las estrellas son vote_average/2)
    this.peliculas = this.peliculas.filter(pelicula => (((pelicula['vote_average']/2) > 4) ));

    //Filtro 2: Que no actue Keanu Reeves
    let aparece=[{}];
      this.peliculas.forEach(function (pelicula) {
        const array_peli = Array(pelicula['cast']);
        for (let index = 0; index < array_peli[0].length; index++) {
             if(array_peli[0][index] == ("Keanu Reeves")){
               aparece.push(pelicula["id"]);
              }
              
            }
            
          }); 
          
      this.peliculas = this.peliculas.filter(pelicula => !aparece.includes(pelicula['id']))
      
      /* 
      El otro filtro, de numero de votos negativos en 2019, no lo he podido hacer porque en la base de datos
      no había ningun registro de los votos negativo ni positivos por años.
      */
            
      
     
      
      
    
    this.numPaginas = (Math.ceil(this.peliculas.length/20));
  

   
      
  }
  
 
}

