import { Component,EventEmitter,Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-pagination-page',
  templateUrl: './pagination-page.component.html',
  styleUrls: ['./pagination-page.component.css']
})
export class PaginationPageComponent implements OnInit {

   @Input() numero: number = 0; 
   @Input() pactual: number = 2; 
   
   @Output() eventCambioPagina = new EventEmitter<number>();  

  constructor() { }

  ngOnInit(): void {
    
  }

  cambiarPagina(){
     this.pactual= this.numero; 
    this.eventCambioPagina.emit(this.pactual);
  }

}
