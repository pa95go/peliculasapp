import { Component, OnInit,EventEmitter,Output, Input } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {
  
  @Input() totalPaginas: number = 5;
  @Output() eventCambioPagina = new EventEmitter<number>();  

  numero: number = 0;
  paginas = new Array();
  paginaActual: number = 1;



  constructor() { }

  ngOnInit(): void {
   /*  for (let index = 1; index <= this.totalPaginas; index++) {
      this.paginas.push(index);
      
    } */
  }

  ngDoCheck(): void {
    this.paginas = [];
    for (let index = 1; index <= this.totalPaginas; index++) {
      this.paginas.push(index);
      
    }
  }



  prueba(){
    console.log('prueba')
  }

  siguiente(){
    if(this.paginaActual<= this.totalPaginas){
      ++this.paginaActual;
      this.eventCambioPagina.emit(this.paginaActual);
    }
  }
  anterior(){
    if(this.paginaActual>= 1){
      --this.paginaActual;
      this.eventCambioPagina.emit(this.paginaActual);
    }
  }

  nuevasPeliculas(){
    this.eventCambioPagina.emit(this.paginaActual);
  }

}