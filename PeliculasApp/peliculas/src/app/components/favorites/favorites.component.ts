import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.css']
})
export class FavoritesComponent implements OnInit {
  
  nombre: string ='Nombre Pelicula';
  imagen: string ='Imagen Pelicula';
  id: string ='0';
  peliculasPagina: number = 1;
  numPaginas: number =1;
  texto:string = '(orden: mayor a menor)';
  peliculas = [];
  listaFavoritas =(String(localStorage.getItem("favoritas")).split('-'));

  constructor(public dataService: DataService) { }

  ngOnInit(): void {

    const url = 'http://localhost:3000/peliculas';
    // obtengo datos utilizando fetch
    fetch(url).then(response => response.json()).then(data => {
      this.peliculas = data.results; // <-- asigno los valores a la propiedad del componente
      
      this.peliculas = this.peliculas.filter(pelicula => this.listaFavoritas.includes(String(pelicula['id'])))
      this.peliculas.sort((a, b) => (a['vote_average'] > b['vote_average'] ? -1 : 1));
      this.numPaginas = (this.peliculas.length/20);
      
      
    });


  }

  reset(){
    localStorage.removeItem("favoritas");
    this.peliculas = [];
  }

}
