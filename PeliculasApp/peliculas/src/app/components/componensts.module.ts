import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { ListComponent } from './list/list.component';
import { PaginationComponent } from './pagination/pagination.component';
import { PaginationPageComponent } from './pagination-page/pagination-page.component';
import { FavoritesComponent } from './favorites/favorites.component';




@NgModule({
  declarations: [
    NavbarComponent,
    ListComponent,
    PaginationComponent,
    PaginationPageComponent,
    FavoritesComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    NavbarComponent,
    ListComponent,
    PaginationComponent
  ]

})
export class ComponenstsModule { }
