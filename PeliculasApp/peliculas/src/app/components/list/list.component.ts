import { Component, Input, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  @Input() id: string = "0";
  @Input() titulo: string = "Pelicula sin titulo";
  @Input() imagen: string = "Pelicula sin imagen";
  @Input() pelicula=[] ;
  @Input() index: number = 0;

  
  constructor(public dataService: DataService) { }
  
  ngOnInit(): void {
   
    
  }
  enviarPelicula(){
  
   localStorage.setItem("pelicula", JSON.stringify(this.pelicula));
    
  }
}
