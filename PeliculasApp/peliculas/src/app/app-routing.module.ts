import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FavoritesComponent } from './components/favorites/favorites.component';
import { HomeComponent } from './pages/home/home.component';
import { MovieComponent } from './pages/movie/movie.component';

const routes: Routes = [
  {path:'home', component: HomeComponent},
  {path:'user/favorites', component: FavoritesComponent}, // hacer un component para el login
  {path:'movie', component: MovieComponent}, // hacer un component para el login
  {path:'**', pathMatch:'full', redirectTo:'home' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
