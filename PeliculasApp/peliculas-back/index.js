
const express =require('express');
const cors = require('cors');
const app = express();
app.use(cors());


const puerto = 3000;

const http = require('http');

let array_pelis= [];

for (let index = 1; index <= 5; index++) {
    
    const options={
        hostname:'api.themoviedb.org',
        path: `/3/discover/movie?sort_by=vote_count.desc&language=es-ES&include_image_language=es&api_key=e49f17139c85d66bdcd374f0fbfffb3f&page=${index}`,
        method: 'GET'
    };
    
    let body = '';
    const req = http.request(options, res=>{
        
        res.on('data', chunk =>{
            body +=chunk;
        });
        res.on('end',()=>{
       
        let jbody = JSON.parse(body);
            for (let index = 0; index < jbody['results'].length; index++) {
                generarArray(jbody['results'][index]);
            }
        })
    });

    req.on('error', err =>{});
    req.end(); 
}


function generarArray(array) { // conseguir el cast de actores y formar el JSON
    

   const options={
    hostname:'api.themoviedb.org',
    path: `/3/movie/${array['id']}/credits?api_key=e49f17139c85d66bdcd374f0fbfffb3f&language=es-ES`,
    method: 'GET'
};
let body = '';
const req = http.request(options, res=>{
    res.on('data', chunk =>{
        body +=chunk;
     });
    res.on('end',()=>{
        let jbody_cast =JSON.parse(body);
       let array_cast=[];
       jbody_cast['cast'].forEach(actor => {
           array_cast.push(actor['name']);

       });
        array_pelis.push({
            id: array['id'], 
            poster_path: array['poster_path'], 
            title: array['title'], 
            original_title: array['original_title'], 
            release_date: array['release_date'], 
            overview: array['overview'], 
            popularity: array['popularity'], 
            vote_average: array['vote_average'], 
            vote_count: array['vote_count'], 
            cast: array_cast
        });
        
    });
});
req.on('error', err =>{});
req.end(
    
);

}


app.get('/peliculas', (req, res)=>{

 
    res.send({results: array_pelis}); 
    // res.send(jbody['results'][1]['title']); 
    
})
app.get('/filtro', (req, res)=>{


    
})



app.listen(puerto, () => {
});


